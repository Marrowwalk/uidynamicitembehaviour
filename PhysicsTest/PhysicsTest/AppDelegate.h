//
//  AppDelegate.h
//  PhysicsTest
//
//  Created by Kos  on 30/08/15.
//  Copyright © 2015 Kos . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

